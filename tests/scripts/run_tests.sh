#!/bin/sh

PATH=/usr/bin
TMPDIR="$(mktemp -d)"
CWD="$(pwd)"

PACKAGE=pykickstart
BRANCH=

# clone the dist-git tree for this package
cd ${TMPDIR}
fedpkg co ${PACKAGE}
cd ${PACKAGE}
[ -z "${BRANCH}" ] || fedpkg switch-branch ${BRANCH}
fedpkg prep

# scramble together the extracted source tree name
SRCDIR="${PACKAGE}-$(grep Version: ${PACKAGE}.spec | cut -d ' ' -f 2)"

# run the tests
cd ${SRCDIR}
make check
CHECK_RET=$?
make test
TEST_RET=$?
RET=$((${CHECK_RET} + ${TEST_RET}))

# clean up and exit
cd ${CWD}
rm -rf ${TMPDIR}
exit ${RET}
